
 var order = { customer : [], items: [] };	

$('#addItem').click(function(){
	var split = document.getElementById('item').value.split("_");
	var quantity = document.getElementById('quantity').value;
	var exist = false;
	// split[0] - item id
	// split[1] - item name

	for(var x=0; x<order.items.length; x++){
		if(order.items[x].id === split[0]){
			exist = true;
			order.items[x].quantity = parseInt(order.items[x].quantity) + parseInt(quantity);
		}
	}

	if(exist == false){
		order.items.push({ 
	     'id' : split[0],
	     'name' : split[1],
	     'price' : split[2],
	     'quantity' : quantity,
	  });
	}
	writeData();
});

function writeData(){
	var tr = "";
	document.getElementById('tbody').innerHTML = "";
	for(var i=0; i<order.items.length; ++i){
		tr = "<tr id='"+order.items[i].id+"'>" +
			"<td class='"+order.items[i].id+"'>"+order.items[i].id+"</td>"+
			"<td class='"+order.items[i].name+"'>"+order.items[i].name+"</td>"+
			"<td class='"+order.items[i].quantity+"'>"+order.items[i].quantity+"</td>"+
			"<td><i class='fa fa-trash-o fa-fw' onclick='deleteItems("+i+")' href='' ></i> </td>"+
		 "</tr>";
		 document.getElementById('tbody').innerHTML += tr;
	}
}

function deleteItems(index){
	order.items.splice(index, 1);
	writeData();
}


$('.customerInfo').click(function(){
	var len = order.customer.length;
	var customer_info = $('#customer_info').val().split('_');
	var id = customer_info[0];
	var name = customer_info[1];
	var address = customer_info[2];
	var orderSlip = $('#orderSlip').val();
	var recievingDate = $('#recievingDate').val();	

	if(len > 0){
		order.customer[0].id = id;
		order.customer[0].name = name;
		order.customer[0].address = address;
		order.customer[0].recievingDate = recievingDate;
	}else{
		order.customer.push({
			'id' : id,
			'name' : name,
			'address' : address,
			'orderSlip' : orderSlip,
			'recievingDate' : recievingDate,
		});
	}
});


$('.review').click(function(){
	$('#receiverID').text(order.customer[0].id);
	$('#recieverName').text(order.customer[0].name);
	$('#recieverAddress').text(order.customer[0].address);
	document.getElementById('tbodyReview').innerHTML = "";
	var tr = "";
	var total = 0;

	for(var x=0; x<order.items.length; x++){
		var totalPerItem = parseFloat(order.items[x].quantity) * parseFloat(order.items[x].price); 
		tr = "<tr>"+
				"<td>"+order.items[x].id+"</td>"+
				"<td>"+order.items[x].name+"</td>"+
				"<td>"+order.items[x].price+".00</td>"+
				"<td>"+order.items[x].quantity+"</td>"+
				"<td>"+totalPerItem+".00</td>"+
			 "</tr>";
		document.getElementById('tbodyReview').innerHTML +=tr;	 
		total += totalPerItem;
	}
	$('#total').text(total);
});

$('.placeOrder').click(function(){
	var backslash = $('.backslash').val();
	var o = JSON.stringify(order);

	if(order.customer[0].orderSlip == "" && order.customer[0].recievingDate == ""){
		alert("You can not leave blank fields!\n Please Complete Customer Information");
	}else if(order.items.length == 0){
		alert("You have no items added!\n Add atleast one item.");
	}else{
		 $.ajax({
            type: "POST",
            url: backslash+"order/save/",
            data: {'order' : o},
            success: function(data){
               alert("Placed Successfully!");
               window.location = backslash + 'order/';
              
              }
        });
	}
});


$('.delOrderT').click(function(){
	var backslash = $('.backslash').val();
	var id = $(this).attr('id');

	var confirmDelete = confirm("Confirm delete!");
	if(confirmDelete == true){
			$.ajax({
			type : "POST",
			url : backslash + "order/delete",
			data : {'id' : id},
			success : function(data){
				if(data == 1){
					alert("Deleted Successfully!");
					window.location = backslash + 'order/';
				}else{
					alert("Failed! \nYou can not delete order used by other table.");
				}
			}
		});
	}	
});

$('.viewOrderT').click(function(){
	var backslash = $('.backslash').val();
	var id = $(this).attr('id');
	window.location = backslash+"order/view/"+id;
});

$('.deleteCustomer').click(function(){
	var backslash = $('.backslash').val();
	var customerID = $(this).attr('id');
	var conf = confirm("Confirm Delete!");
	if(conf==true){
		$.ajax({
			type : "POST",
			url : backslash + "customer/delete",
			data : {'customerID' : customerID },
			success : function(data){
				if(data == 1){
					alert("Deleted Successfully! ");
					window.location = backslash + "customer";
				}else{
					alert("Failed! \nYou can not delete customer used by another table.");
				}
			}
		});
	}
});

$('.deleteMerchandise').click(function(){
	var backslash = $('.backslash').val();
	var mid = $(this).attr('id');
	var conf = confirm("Confirm Delete!");
	if(conf==true){
		$.ajax({
			type : "POST",
			url : backslash + "merchandise/delete",
			data : {'merchandiseID' : mid },
			success : function(data){
				if(data == 1){
					alert("Deleted Successfully!");
					window.location = backslash + "merchandise";
				}else{
					alert("Failed! \nYou can not delete merchandise used by another table.");
				}
			}
		});
	}
});

$('.updateCustomer').click(function(){
	var backslash = $('.backslash').val();
	var id = $(this).attr('id');
	window.location = backslash + "customer/update/"+id;
});

$('.updateMerchandise').click(function(){
	var backslash = $('.backslash').val();
	var id = $(this).attr('id');
	window.location = backslash + "merchandise/update/"+id;
});