<?php require_once'app/mvc/view/common/head.php'; ?>
<?php
	$brand = 'Orders';
	$settings = 'active';
    $employee_caret = 'fa fa-caret-right';
    $settings_caret = 'fa fa-caret-down';
    echo "<input type='hidden' class='backslash' value='".$backslash."'/>";
?>
<?php require_once'app/mvc/view/common/sidebar.php'; ?>
      <?php require_once'app/mvc/view/common/nav.php'; ?>
             <div class="container-fluid">
                   <div class="container col-md-12">   
                      <style type="text/css">
                      	.details {
                      		margin-right :10px;
                      	}
                      	label {
                      		float :left;
                      	}
                  	  </style>
                      <!-- content -->
                        <!-- Nav tabs -->
                        <br/>
                        <ul class="nav nav-tabs">
                          <li class="nav-item">
                            <a class="nav-link fa fa-arrow-left" href="<?php echo $backslash;?>order"> Back</a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#viewDepartment">Review Orders</a>
                          </li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                          <div class="tab-pane container active" id="viewDepartment">
                            <br/>
                            <h6>Customer Information</h6>
                            <table class="table table-hover">
                                <thead>   
                                     <th>
                                        Customer ID
                                    </th>
                                    <th>
                                        Customer Name
                                    </th>
                                    <th>
                                        Customer Address
                                    </th>
                                </thead>
                                <tbody>
                                  <?php
                                  $customer = (object) $data[0];
                                  $customer = $customer;
                                      if($customer != NULL){
                                  ?>
                                    <tr>
                                        <td> <?php echo $customer->customerID;?></td>
                                        <td> <?php echo $customer->customerName;?></td>
                                        <td> <?php echo $customer->customerAddress;?></td>
                                    </tr>
                                  <?php
                                     }
                                  ?>
                                    
                                </tbody>
                           </table>
                           <h6>Order Details</h6>
                            <table class="table table-hover">
                                <thead>   
                                     <th>
                                        Item Name
                                    </th>
                                    <th>
                                        Quantity
                                    </th>
                                    <th>
                                        Unit Price
                                    </th>
                                    <th>
                                        Total
                                    </th>
                                   
                                </thead>
                                <tbody>
                                  <?php
                                  	  $total = 0;
                                  	  $prdct = 0;
	                                  $data = (object) $data;
	                                  $OrderT = $data;
	                                      if($OrderT != NULL){
	                                      foreach($OrderT as $order){ 
	                                     	 $prdct = $order->unitPrice * $order->qty;
	                                     	 $total = $total + $prdct;
	                                  ?>
                                    <tr>       
                                        <td> <?php echo $order->merchandiseName;?></td>
                                        <td> <?php echo $order->qty;?></td>
                                        <td> <?php echo $order->unitPrice;?>.00</td>
                                        <td> <?php echo $prdct;?>.00</td>
                                    </tr>
                                  <?php
                                      }  
                                  ?>
                                  	<tr class="total">       
                                        <td>Total</td>
                                        <td></td>
                                        <td></td>
                                        <td><?php echo $total; ?>.00</td>
                                    </tr>
                                  <?php
                                  	  } 
                                  ?>  
                                </tbody>
                           </table>
                          </div>
                          <!-- end view -->
                        </div>
                        <!-- end content -->
                </div>
            </div>
<?php require_once'app/mvc/view/common/footer.php'; ?>