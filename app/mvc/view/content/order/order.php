<?php require_once'app/mvc/view/common/head.php'; ?>
<?php
	$brand = 'Orders';
	$settings = 'active';
    $employee_caret = 'fa fa-caret-right';
    $settings_caret = 'fa fa-caret-down';
?>
<?php require_once'app/mvc/view/common/sidebar.php'; ?>
      <?php require_once'app/mvc/view/common/nav.php'; ?>
             <div class="container-fluid">
                   <div class="container col-md-12">   
                      <style type="text/css">
                      	.details {
                      		margin-right :10px;
                      	}
                      	label {
                      		float :left;
                      	}
                  	  </style>
                      <!-- content -->
                        <!-- Nav tabs -->
                        <br/>
                        <ul class="nav nav-tabs">
                          <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#addDepartment">Add Order</a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#viewDepartment">View Orders</a>
                          </li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <!-- add -->
                            <br/>
                          <div class="tab-pane container" id="addDepartment">


                               <form id="myform" action="<?php echo $backslash; ?>customer/save/" method="POST" class="form-group">
                                 
				 				<ul id="progressbar" class="col-xs-12" >
				                  <li class="active col-md-4">Customer Information</li>
				                  <li class="col-md-4">Order Details</li>
				                  <li class="col-md-4">Review Order</li>
				           
				                </ul>
				                <fieldset class="fieldstyle col-xs-11 col-sm-11 col-sm-offset-1 col-md-11 col-md-offset-1">
				                  <div class="details">
				                   Customer Information
				                  </div>
				  
				                  <div class="col-xs-12 col-md-10">
				                     <label class="control-label" for="Orders">Customer</label>
				                          <select id="customer_info" class="form-control col-sm-12">
				                          	 <?php
				              					  $data = (object) $data;
				                                  $customers = $data->customer;
				                                      if($customers != NULL){
				                                      foreach($customers as $customer){ 
				                                  ?>
				                          				<option value="<?php echo $customer->customerID;?>_<?php echo $customer->customerName;?>_<?php echo $customer->customerAddress;?>">
				                          							   <?php echo $customer->customerName;?>			
				                          				</option>
				                                  <?php
				                                      }  }
				                                  ?>
				                          </select>

				                           <label class="control-label" for="merchandiseName">Order Slip</label>
				                          <input name="orderSlip" class="form-control" id="orderSlip" autocomplete="off" autocomplete="false" placeholder="123" />

				                           <label class="control-label" for="merchandiseName">Order Recieving Date</label>
				                          <input name="recievingDate" class="form-control" id="recievingDate" autocomplete="off" autocomplete="false" placeholder="YYYY-MM-DD" />
				                                              
				                  </div>
				                    <br/>
				                  <input class="btn btn-secondary next customerInfo col-xs-12 col-sm-12 col-md-4" type="button" value="Next">
				                </fieldset>

				                <fieldset class="fieldstyle col-xs-11 col-sm-11 col-sm-offset-2 col-md-11 col-md-offset-3">
				                  <div class="details">
				                    Order Details
				                  </div>
				                  <div class="col-xs-12 col-sm-12">
				                  	<div class="form-inline">
				                  		<div class="col-sm-5">
			                   				 <label class="control-label " for="merchandiseID">Item </label>
					                    </div>
					                    <div class="col-sm-4">
				                         	<label class="control-label" for="merchandiseName">Quantity</label>
				                        </div>
				                     	<div class="col-sm-4"></div>
				                  	</div>

			                   		<div class="form-inline col-sm-12 container-fluid">
			                   				
		                   				<select id="item" class="form-control col-sm-5 details">
				                      	 <?php
				          					  $data = (object) $data;
				                              $merchandises = $data->merchandise; 
				                              var_dump($merchandises);
				                                  if($merchandises != NULL){
				                                  foreach($merchandises as $merchandise){ 
				                              ?>
				                      				<option value="<?php echo $merchandise->merchandiseID;?>_<?php echo $merchandise->merchandiseName;?>_<?php echo $merchandise->unitPrice;?>"> <?php echo $merchandise->merchandiseName;?> </option>
				                              <?php
				                                  }  }
			                              ?>
			                     	    </select>	
	                   					<input name="quantity" class="form-control col-sm-2 details" id="quantity" autocomplete="off" autocomplete="false" placeholder="5" value="1" />
	                   					 <input class="btn btn-primary col-sm-1 details" id="addItem" type="button" value="Add"/>
				                  	</div>
				                  </div>
									<table class="table table-striped">
									    <thead>
									      <tr>
									        <th>MerchandiseID</th>
									      	<th>MerchandiseName</th>
									      	<th>Quantity</th>
									      	<th>Remove</th>
									      </tr>
									    </thead>
									    <tbody id="tbody">
									    </tbody>
									  </table>
				                  <hr/>
				                  <input class="btn btn-secondary previous col-xs-12 col-sm-12 col-md-4" type="button" value="Previous">
				                   <input class="btn btn-secondary next col-xs-12 col-sm-12 col-md-4 review" type="button" value="Next">
				                </fieldset>

				                <fieldset class="fieldstyle col-xs-11 col-sm-11 col-sm-offset-2 col-md-11 col-md-offset-3">
				                  <h6>Recivers Information</h6>
				                   <table class="table table-striped">
										    <thead>
										      <tr>
										        <th>ID</th>
										      	<th>Name</th>
										      	<th>Address</th>
										      </tr>
										    </thead>
										    <tbody>
										    	<td id="receiverID"></td>
										    	<td id="recieverName"></td>
										    	<td id="recieverAddress"></td>
										    </tbody>
									  </table>
									   <h6> Order Details </h6>
									  <table class="table table-striped">
										    <thead>
										      <tr>
										        <th>MerchandiseID</th>
										      	<th>MerchandiseName</th>
										      	<th>Unit Price</th>
										      	<th>Quantity</th>
										      	<th>Amount</th>
										      </tr>
										    </thead>
										    <tbody id="tbodyReview">
										    	
										    </tbody>
										    <tbody>
										    	<tr>
										    		<td>Total</td>
										    		<td></td>
										    		<td></td>
										    		<td></td>
										    		<td><span id="total"></span>.00</h4></td>
										    	</tr>
										    </tbody>
									  </table>
									 
				                  <p></p>
				                  <p>
				                  
				                  <p></p>
				                 <input class="btn btn-secondary previous col-xs-12 col-sm-12 col-md-4" type="button" value="Previous">        
				                  <input class="btn btn-success col-xs-12 col-sm-12 col-md-4 placeOrder" type="button" value="Place Order"/>
				                </fieldset>
                               </form>
                          </div>
                          <!-- end add -->

                          <!-- view -->
                          <div class="tab-pane container active" id="viewDepartment">
                            <br/>
                            <table class="table table-hover" id="employeeTable">
                                <thead>
                                    <th>
                                        OrderSlipID
                                    </th>
                                     <th>
                                        Customer Name
                                    </th>
                                     <th>
                                        Order Receiving Date
                                    </th>
                                    <th>
                                        ACTION
                                    </th>
                                </thead>
                                <tbody>
                                  <?php
	                                  $data = (object) $data;
	                                  $OrderT = $data->OrderT;
	                                      if($OrderT != NULL){
	                                      foreach($OrderT as $order){ 
	                                  ?>
                                    <tr>
                                        <td> <?php echo $order->orderSlipID;?></td>
                                        <td> <?php echo $order->customerName;?></td>
                                        <td> <?php echo $order->orderReceivingDate;?></td>
                                        <td> <i id="<?php echo $order->orderSlipID;?>" class="fa fa-search viewOrderT fa-fw"></i> <i id="<?php echo $order->orderSlipID;?>" class="fa fa-trash-o delOrderT fa-fw"></i></td>
                                    </tr>
                                  <?php
                                      }  }
                                  ?>
                                    
                                </tbody>
                           </table>
                          </div>
                          <!-- end view -->
                        </div>
                        <!-- end content -->
                </div>
            </div>
<?php require_once'app/mvc/view/common/footer.php'; ?>