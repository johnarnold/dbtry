<?php require_once'app/mvc/view/common/head.php'; ?>
<?php
	$brand = 'Merchandise';
	$settings = 'active';
    $employee_caret = 'fa fa-caret-right';
    $settings_caret = 'fa fa-caret-down';
?>
<?php require_once'app/mvc/view/common/sidebar.php'; ?>
      <?php require_once'app/mvc/view/common/nav.php'; ?>
             <div class="container-fluid">
                   <div class="container col-md-12">   
                      
                      <!-- content -->
                        <!-- Nav tabs -->
                        <br/>
                        <ul class="nav nav-tabs">
                          <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#addDepartment">Add Merchandise</a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#viewDepartment">View Merchandise</a>
                          </li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <!-- add -->
                            <br/>
                          <div class="tab-pane container" id="addDepartment">
                               <form id="myform" action="<?php echo $backslash; ?>merchandise/save/" method="POST">
                                     <div class="container">
                                      <div class="row">
                                          <div class="col-sm-5">
                                            <div class="form-group">
                                              <label class="control-label" for="merchandiseID">Merchandise Id</label>
                                              <input name="merchandiseID" class="form-control" id="merchandiseID" autocomplete="off" autocomplete="false" value="" />
                                              <label class="control-label" for="merchandiseName">Merchandise</label>
                                              <input name="merchandiseName" class="form-control" id="merchandiseName" autocomplete="off" autocomplete="false" placeholder="Merchandise Name" />
                                              <label class="control-label" for="address">Customer Address</label>
                                              <textarea name="unitPrice" class="form-control" id="unitPrice" autocomplete="off" autocomplete="false" placeholder="Price"></textarea>
                                            </div>
                                             <input class="btn btn-primary col-xs-12 col-sm-12 col-md-4" type="submit" submit="submit" name="submit" value="Save" />
                                      </div>
                                     </div>
                                 </div>
                               </form>
                          </div>
                        
                          <!-- end add -->

                          <!-- view -->
                          <div class="tab-pane active container" id="viewDepartment">
                            <br/>
                            <table class="table table-hover" id="employeeTable">
                                <thead>
                                    <th>
                                        ID
                                    </th>
                                     <th>
                                        Merchandise
                                    </th>
                                     <th>
                                        Price
                                    </th>
                                    <th>
                                        ACTION
                                    </th>
                                </thead>
                                <tbody>
                                  <?php
                                      if($data != NULL){
                                      foreach($data as $Merchandise){ 
                                  ?>
                                    <tr>
                                        <td> <?php echo $Merchandise->merchandiseID;  ?></td>
                                        <td> <?php echo $Merchandise->merchandiseName;  ?></td>
                                        <td> <?php echo $Merchandise->unitPrice;  ?>.00</td>
                                        <td>  <i id="<?php echo $Merchandise->merchandiseID; ?>" class="fa fa-pencil-square-o fa-fw updateMerchandise"></i> <i id="<?php echo $Merchandise->merchandiseID; ?>" class="fa fa-trash-o fa-fw deleteMerchandise"></i></td>
                                    </tr>
                                  <?php
                                      }  }
                                  ?>
                                    
                                </tbody>
                           </table>
                          </div>
                          <!-- end view -->
                        </div>
                        <!-- end content -->
                </div>
            </div>
<?php require_once'app/mvc/view/common/footer.php'; ?>
