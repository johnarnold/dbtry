<?php require_once'app/mvc/view/common/head.php'; ?>
<?php
	$brand = 'Customer / Update';
	$settings = 'active';
    $employee_caret = 'fa fa-caret-right';
    $settings_caret = 'fa fa-caret-down';
?>
<?php require_once'app/mvc/view/common/sidebar.php'; ?>
      <?php require_once'app/mvc/view/common/nav.php'; ?>
             <div class="container-fluid">
                   <div class="container col-md-12">   
                      <!-- content -->
                        <!-- Nav tabs -->
                        <br/>
                        <ul class="nav nav-tabs">
                          <li class="nav-item">
                            <a class="nav-link fa fa-arrow-left" href="<?php echo $backslash;?>customer"> Back</a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#viewDepartment">Update Customer</a>
                          </li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <!-- add -->
                            <br/>
                          <div class="tab-pane container" id="addDepartment">
                               
                          </div>
                        
                          <!-- end add -->

                          <!-- view -->
                          <div class="tab-pane active container" id="viewDepartment">
                            <br/>
                            <form id="myform" action="<?php echo $backslash; ?>customer/save/" method="POST">
                                     <div class="container">
                                      <div class="row">
                                          <div class="col-sm-5">
                                            <div class="form-group">
                                              <label class="control-label" for="merchandiseID">Customer Id</label>
                                              <input name="customerId" class="form-control" id="merchandiseID" autocomplete="off" autocomplete="false" value="<?php echo $data[0]->customerID;?>" placeholder="Id" />
                                              <label class="control-label" for="merchandiseName">Customer Name</label>
                                              <input name="customerName" class="form-control" id="merchandiseName" autocomplete="off" autocomplete="false" value="<?php echo $data[0]->customerName;?>" placeholder="Name" />
                                              <label class="control-label" for="address">Customer Address</label>
                                              <textarea name="customerAddress" class="form-control" id="unitPrice" autocomplete="off" autocomplete="false" placeholder="Address"><?php echo $data[0]->customerAddress;?></textarea>
                                            </div>
                                             <input class="btn btn-primary col-xs-12 col-sm-12 col-md-4" type="submit" submit="submit" name="submit" value="Update" />
                                      </div>
                                     </div>
                                 </div>
                               </form>
                          </div>
                          <!-- end view -->
                        </div>
                        <!-- end content -->
                </div>
            </div>
<?php require_once'app/mvc/view/common/footer.php'; ?>