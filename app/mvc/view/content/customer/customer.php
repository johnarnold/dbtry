<?php require_once'app/mvc/view/common/head.php'; ?>
<?php
	$brand = 'Customer';
	$settings = 'active';
    $employee_caret = 'fa fa-caret-right';
    $settings_caret = 'fa fa-caret-down';
?>
<?php require_once'app/mvc/view/common/sidebar.php'; ?>
      <?php require_once'app/mvc/view/common/nav.php'; ?>
             <div class="container-fluid">
                   <div class="container col-md-12">   
                      <!-- content -->
                        <!-- Nav tabs -->
                        <br/>
                        <ul class="nav nav-tabs">
                          <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#addDepartment">Add Customer</a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#viewDepartment">View Customer</a>
                          </li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <!-- add -->
                            <br/>
                          <div class="tab-pane container" id="addDepartment">
                               <form id="myform" action="<?php echo $backslash; ?>customer/save/" method="POST">
                                     <div class="container">
                                      <div class="row">
                                          <div class="col-sm-5">
                                            <div class="form-group">
                                              <label class="control-label" for="merchandiseID">Customer Id</label>
                                              <input name="customerId" class="form-control" id="merchandiseID" autocomplete="off" autocomplete="false" value="" placeholder="Id" />
                                              <label class="control-label" for="merchandiseName">Customer Name</label>
                                              <input name="customerName" class="form-control" id="merchandiseName" autocomplete="off" autocomplete="false" placeholder="Name" />
                                              <label class="control-label" for="address">Customer Address</label>
                                              <textarea name="customerAddress" class="form-control" id="unitPrice" autocomplete="off" autocomplete="false" placeholder="Address"></textarea>
                                            </div>
                                             <input class="btn btn-primary col-xs-12 col-sm-12 col-md-4" type="submit" submit="submit" name="submit" value="Save" />
                                      </div>
                                     </div>
                                 </div>
                               </form>
                          </div>
                        
                          <!-- end add -->

                          <!-- view -->
                          <div class="tab-pane active container" id="viewDepartment">
                            <br/>
                            <table class="table table-hover" id="employeeTable">
                                <thead>
                                    <th>
                                        ID
                                    </th>
                                     <th>
                                        Merchandise
                                    </th>
                                     <th>
                                        Price
                                    </th>
                                    <th>
                                        ACTION
                                    </th>
                                </thead>
                                <tbody>
                                  <?php
                                      if($data != NULL){
                                      foreach($data as $customer){ 
                                  ?>
                                    <tr>
                                        <td> <?php echo $customer->customerID;?></td>
                                        <td> <?php echo $customer->customerName;?></td>
                                        <td> <?php echo $customer->customerAddress;?></td>
                                        <td>  <i id="<?php echo $customer->customerID;?>" class="fa fa-pencil-square-o fa-fw updateCustomer"></i> <i id="<?php echo $customer->customerID;?>" class="fa fa-trash-o fa-fw deleteCustomer"></i></td>
                                    </tr>
                                  <?php
                                      }  }
                                  ?>
                                    
                                </tbody>
                           </table>
                          </div>
                          <!-- end view -->
                        </div>
                        <!-- end content -->
                </div>
            </div>
<?php require_once'app/mvc/view/common/footer.php'; ?>