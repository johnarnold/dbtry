<aside id="sidebar-wrapper">
  <ul class="sidebar-nav metismenu" id="menu">
    <li class="sidebar-brand">
          <!-- <img alt="logo" src="<?php //echo $backslash; ?>app/images/common/hrms.png"></img> -->
    </li>
    <br/>
    <li class="<?php echo $settings; ?>">
      <a href="<?php echo $backslash;?>customer/" class="settings" href="#"><i class="fa fa-user fa-fw"></i> <span>Customer</span>
      <!-- <b class="caret pull-right"></b> <i id="settings" class="fa <?php echo $settings_caret; ?>"></i></a> -->
    </li>
    <li class="<?php echo $travel; ?>">
      <a href="<?php echo $backslash;?>merchandise/"><i class="fa fa-shopping-cart fa-fw"></i> <span>Merchandise</span></a>
    </li>
    <li class="<?php echo $leave; ?>">
      <a href="<?php echo $backslash;?>order/"><i class="fa fa-briefcase fa-fw"></i> <span>Orders</span></a>
    </li>
  </ul>
</aside>
<link rel="stylesheet" href="<?php echo $backslash; ?>app/css/aside/css/sidebar.css">

<script>

  function employeeDropdown(e){

    var classname = document.getElementById(e).className;

        if(classname == "fa fa-caret-right"){

              document.getElementById(e).className = "fa fa-caret-down";
              closeSettings(e);

        }else{

              document.getElementById(e).className = "fa fa-caret-right";
        }

    }

    function settingsDropdown(e){

      var classname = document.getElementById(e).className;

            if(classname == "fa fa-caret-right"){

                  document.getElementById(e).className = "fa fa-caret-down";
                  closeEmployee();

            }else{

                  document.getElementById(e).className = "fa fa-caret-right";
                }
      }


    function closeSettings(){
    
        document.getElementById('settings').className = "fa fa-caret-right";
    }

    function closeEmployee(){
        document.getElementById('employee').className = "fa fa-caret-right";
    }


</script>