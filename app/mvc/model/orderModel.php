<?php
	class orderModel {
		public function __Construct(){
		}

		public function save($data){
			$order = (object) $data;
			$customer = $order->customer[0];

			$id = $customer->id;
			$name = $customer->name;
			$orderSlip = $customer->orderSlip;
			$recievingDate = $customer->recievingDate;
			
			$sql = "INSERT INTO OrderT (
										orderSlipID, 
								   		customerID, 
								   		orderReceivingDate 
							   	        ) 
								   		VALUES (?,?,?)
								   		";
			$stmt = Db::getInstance()->prepare($sql);
			$stmt->bind_param("iss", $orderSlip, $id, $recievingDate);
			$stmt->execute();

			$numberOfItems = count($order->items);
			for($i=0; $i<$numberOfItems; $i++){
				$items = $order->items[$i];
				$sql = "INSERT INTO OrderDetail (
										customerID, 
								   		orderSlipID, 
								   		merchandiseID,
								   		qty 
							   	        ) 
								   		VALUES (?,?,?,?)
								   		";
				$stmt = Db::getInstance()->prepare($sql);
				$stmt->bind_param("sisi", $id, $orderSlip, $items->id, $items->quantity);
				$stmt->execute();
			}


		}

		public function query(){
			$sql = "SELECT OrderT.orderSlipID, Customer.customerName, OrderT.orderReceivingDate FROM OrderT LEFT JOIN Customer ON Customer.customerID = OrderT.customerID"; 
			$result = mysqli_query(Db::getInstance(), $sql);
				if($result->num_rows){
					while($row = $result->fetch_object()){
						$OrderT[] = $row;
					}
				}else{
					$OrderT = array();
				}
			return $OrderT;
		}

		public function queryItems($id){
			$sql = "SELECT * FROM OrderDetail LEFT JOIN Customer ON OrderDetail.customerID = Customer.customerID LEFT JOIN Merchandise ON OrderDetail.merchandiseID = Merchandise.merchandiseID WHERE orderSlipID = $id";
			$result = mysqli_query(Db::getInstance(), $sql);
				if($result->num_rows){
					while($row = $result->fetch_object()){
						$OrderDetail[] = $row;
					}
				}else{
					$OrderDetail = array();
				}
			return $OrderDetail;
		}

		public function delete($id){
			$sql = "DELETE ot, od FROM OrderT ot, OrderDetail od WHERE ot.orderSlipID = ? AND ot.orderSlipID = od.orderSlipID";
			$stmt = Db::getInstance()->prepare($sql);
			$stmt->bind_param("i", $id);
			$stmt->execute();
			if($this->queryItems($id) == null){
				return 1;
			}else{
				return 0;
			}
		}
	}

?>