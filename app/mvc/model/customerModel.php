<?php
	class customerModel {
		public function __Construct(){
			
		}

		public function save($data){
			$data = (object) $data;
			$id = $data->id;
			$name = $data->name;
			$address = $data->address;

			$sql = "INSERT INTO Customer (
										customerID, 
								   		customerName, 
								   		customerAddress 
							   	        ) 
								   		VALUES (?,?,?)
								   		";
			$stmt = Db::getInstance()->prepare($sql);
			$stmt->bind_param("sss", $id, $name, $address);
			$stmt->execute();
		}

		public function update($data){
			$data = (Object) $data;
			$id = $data->id;
			$name = $data->name;
			$address = $data->address;

			$sql = "UPDATE Customer Set customerName = ?, customerAddress = ? WHERE customerID = ?";
			$st = Db::getInstance()->prepare($sql);
			$st->bind_param("sss", $name, $address, $id);
			$st->execute();
		}

		public function query(){
			$sql = "SELECT * FROM Customer ORDER BY customerID DESC";
			$result = mysqli_query(Db::getInstance(), $sql);
					if($result->num_rows){
						while($row = $result->fetch_object()){
							$customer[] = $row;
						}
					}else{
						$customer = array();
					}
			return $customer;
		}

		public function delete($id){
			$sql = "DELETE FROM Customer WHERE NOT EXISTS(SELECT customerID FROM OrderT o WHERE o.customerID=?) AND customerID=?";
			$st = Db::getInstance()->prepare($sql);
			$st->bind_param("ss", $id,$id);
			$st->execute();

			if($this->queryOneCustomer($id) == null){
				return 1;
			}else{
				return 0;
			}
		}

		public function queryOneCustomer($id){
			$sql = "SELECT * FROM Customer WHERE customerID = '$id'";
			$result = mysqli_query(Db::getInstance(), $sql);
					if($result->num_rows){
						$customer[] = $result->fetch_object();
					}else{
						$customer = array();
					}
			return $customer;
		}
	}

?>