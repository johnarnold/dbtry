<?php

class merchandiseModel {

	public function __Construct(){

	}

	public function save($data){
		$data =  (object) $data;
		$merchandiseID = $data->merchandiseID;
		$merchandiseName = $data->merchandiseName;
		$unitPrice = $data->unitPrice;
		$sql = "INSERT INTO Merchandise (
										merchandiseID, 
								   		merchandiseName, 
								   		unitPrice 
							   	        ) 
								   		VALUES (?,?,?)
								   		";
		$stmt = Db::getInstance()->prepare($sql);
		$stmt->bind_param("sss", $merchandiseID, $merchandiseName, $unitPrice);
		$stmt->execute();
	}

	public function update($data){
		$data =  (object) $data;
		$merchandiseID = $data->merchandiseID;
		$merchandiseName = $data->merchandiseName;
		$unitPrice = $data->unitPrice;
		$sql = "UPDATE Merchandise SET merchandiseName = ?, unitPrice = ? WHERE merchandiseID = ?";
		$st = Db::getInstance()->prepare($sql);
		$st->bind_param("sss", $merchandiseName, $unitPrice, $merchandiseID);
		$st->execute();
	}

	public function queryOneItem($id){
		$sql = "SELECT * FROM Merchandise WHERE merchandiseID = '$id'";
		$rs = mysqli_query(Db::getInstance(), $sql);
			if($rs->num_rows){
				$Merchandise[] = $rs->fetch_object();
			}else{
				$Merchandise = array();
			}
			
		return $Merchandise;	
	}

	public function query(){
		$sql = "SELECT * FROM Merchandise";
		$result = mysqli_query(Db::getInstance(), $sql);
				if($result->num_rows){
					while($row = $result->fetch_object()){
						$Merchandise[] = $row;
					}
				}else{
					$Merchandise = array();
				}
		return $Merchandise;
	}

	public function delete($id){
		$sql = "DELETE FROM Merchandise WHERE NOT EXISTS(SELECT merchandiseID FROM OrderDetail o WHERE o.merchandiseID=?) AND merchandiseID = ?";
		$st = Db::getInstance()->prepare($sql);
		$st->bind_param("ss", $id, $id);
		$st->execute();
		if($this->queryOneItem($id) == null){
			return 1;
		}else{
			return 0;
		}
	}
}

?>