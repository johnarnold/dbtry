<?php

use controller\Controller;

class order extends Controller {
	private $controller;
	private $url;
	private $auth;
	private $backslash;
	private $data;

	public function __Construct($url, $backslash){
		$this->controller = new controller($url);
		$this->auth = $this->controller->authentication;
		$this->backslash = $backslash;
		$this->trash = 0;
		$this->url = $url;
	}

	public function order(){
		$controller = new Controller('customer');
		$customer = $controller->Model('customer')->query();

		$controller = new Controller('merchandise');
		$merchandise = $controller->Model('merchandise')->query();

		$execute = $this->controller->Model($this->url);
		$OrderT = $execute->query();

		$this->data = array("customer" => $customer, "merchandise" => $merchandise, "OrderT" => $OrderT);

		$this->display($this->url);
	}

	public function view_withParam($id){
		$controller = new Controller($this->url);
		$this->data = $controller->Model($this->url)->queryItems($id);
		$this->display('viewOrder');
	}

	public function save(){
		$order = json_decode($_POST['order']);
		$execute = $this->controller->Model($this->url);
		$execute->save($order);
	}

	public function edit(){


	}

	public function update(){

	}

	public function delete(){
		$id = $_POST['id'];
		$execute = $this->controller->Model($this->url);
		$response = $execute->delete($id);
		echo $response;
	}

	public function display($page){
		$this->controller->render->view("content/order/".$page, $this->backslash, $this->data);
	}

	private function query(){

	}

}


?>