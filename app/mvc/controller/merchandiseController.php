<?php

use controller\controller;

class merchandise extends Controller{
	private $controller = NULL;
	private $auth = NULL;
	private $data = NULL;
	public $backslash = null;
	private $trash = null;
	private $url = null;

	public function __Construct($url, $backslash){
		$this->controller = new controller($url);
		$this->auth = $this->controller->authentication;
		$this->backslash = $backslash;
		$this->trash = 0;
		$this->url = $url;
	}

	public function merchandise(){
		$this->data = $this->query();
		$this->display('merchandise');
	}

	public function save(){
		$data = array("merchandiseID" => $_POST['merchandiseID'],
					  "merchandiseName" => $_POST['merchandiseName'],
					  "unitPrice" => $_POST['unitPrice']
					 );
		$execute = $this->controller->Model($this->url);
		$check = $execute->queryOneItem($_POST['merchandiseID']);
		if($check != NULL){
			$execute->update($data);
		}else{
			$execute->save($data);
		}
		$this->data = $this->query();
		$this->display('merchandise');
	}

	public function update(){

	}

	public function update_withParam($id){
		$execute = $this->controller->Model($this->url);
		$this->data = $execute->queryOneItem($id);
		$this->display("updateMerchandise");
	}

	public function delete(){
		$id = $_POST['merchandiseID'];
		$execute = $this->controller->Model($this->url);
		$data = $execute->delete($id);
		echo $data;
	}

	public function query(){
		$execute = $this->controller->Model($this->url);
		$data = $execute->query();
		return $data;
	}

	public function display($page){
		$this->controller->render->view('content/merchandise/'.$page, $this->backslash, $this->data);
	}

}

?>