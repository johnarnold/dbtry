<?php
	use controller\Controller;

	class customer extends Controller {
		private $controller;
		private $url;
		private $auth;
		private $backslash;
		private $data;

		public function __Construct($url, $backslash){
			$this->controller = new controller($url);
			$this->auth = $this->controller->authentication;
			$this->backslash = $backslash;
			$this->trash = 0;
			$this->url = $url;
		}

		public function customer(){
			$this->data = $this->query();
			$this->display('customer');
		}

		public function save(){
			$data = array("id" => $_POST['customerId'],
						  "name" => $_POST['customerName'],
						  "address" => $_POST['customerAddress']
						 );	
				$execute = $this->controller->Model($this->url);
				$check = $execute->queryOneCustomer($_POST['customerId']);

				if($check != NULL){
					$execute->update($data);
				}else{
					$execute->save($data);
				}
				$this->data = $this->query();
				$this->display('customer');
		}

		public function update(){

		}

		public function update_withParam($id){
			$execute = $this->controller->Model($this->url);
			$this->data = $execute->queryOneCustomer($id);
			$this->display('updateCustomer');
		}

		public function query(){
			$execute = $this->controller->Model($this->url);
			$data = $execute->query();
			return $data;
		}

		public function delete(){
			$id = $_POST['customerID'];
			$execute = $this->controller->Model($this->url);
			$data = $execute->delete($id);
			echo $data;
		}

		public function display($page){
			$this->controller->render->view('content/customer/'.$page, $this->backslash, $this->data);
		}


	}


?>