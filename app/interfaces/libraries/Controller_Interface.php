<?php

namespace Interfaces\Controller\Lib;

Interface Controller_Interface {
	
	public function Model($modelPath);
	public function save();
	public function update();	
	public function delete();
	public function display($page);

}

?>