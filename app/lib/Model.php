<?php

namespace Model;
use Interfaces\Model\Lib\Model_Interface;
use File\Check;

class Model implements Model_Interface {

	private $modelInstance = NULL;

	public function __Construct($path){

		  if(Check::if_File_exist('app/mvc/model/'.$path.'Model.php') === true){

				 require_once'app/mvc/model/'.$path.'Model.php';
				 $model = $path.'Model';
				 $this->modelInstance = new $model();


		  }else{
				
		  		 $this->modelInstance = NULL;
		  }
	}

	public function getModelInstance(){

		return $this->modelInstance;
	}

}


?>