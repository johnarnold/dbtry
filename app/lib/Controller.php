<?php

namespace Controller;

use Model\model;
use View\view;
use Session\session;
use Authentication\authentication;
use Interfaces\Controller\Lib\Controller_Interface;

class Controller implements Controller_Interface {
       
    protected $execute;
    protected $render;
    protected $authentication;

    public function __Construct($path){
   
        $this->render = new view();

        if(empty($_SESSION)){
              session::start();
        }

        $auth = new authentication();
        $auth->set_authentication(Session::get('user','username'),
                                  Session::get('user','password'),
                                  Session::get('user','user_type')
                                 );

        $this->authentication = $auth;
        //echo Session::get('user','user_username');
        //session::display();
   }

   public function Model($modelPath){

        $model = new Model($modelPath);
        $this->execute = $model->getModelInstance();
        return $this->execute;
   }       

   public function save(){

   }

   public function update(){

   }

   public function delete() {

   }

   public function display($page){

   }

}
?>