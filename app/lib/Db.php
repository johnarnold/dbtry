<?php
use Interfaces\Db\Lib\Db_Interface;

	class Db implements Db_Interface {

	private static $connection = NULL;

	public function __Construct(){
	}

	public static function getInstance() {

	$database = new database_configuration();
	$myHost =  $database->getDbConfiguration()['host'];
	$myUserName =  $database->getDbConfiguration()['username'];
	$myPassword =  $database->getDbConfiguration()['password'];
	$myDataBaseName =  $database->getDbConfiguration()['database'];

      if (!isset(self::$connection)) {
       
      		$con = mysqli_connect( "$myHost", "$myUserName", "$myPassword", "$myDataBaseName" );

				if( !$con ) // == null if creation of connection object failed
					{ 
		   			 // report the error to the user, then exit program
		   			 die("connection object not created: ".mysqli_error($con));
					
					}
			
				if( mysqli_connect_errno() )  // returns false if no error occurred
					{ 
		   			 // report the error to the user, then exit program
		    			die("Connect failed: ".mysqli_connect_errno()." : ". mysqli_connect_error());
					}

        self::$connection = $con;
      }

      
      return self::$connection;
    }
	
}

?>