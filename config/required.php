<?php

class required{

	public function __Construct(){
		
		$directories = array('app/interfaces/libraries', 
							 'app/interfaces/controller', 
							 'app/interfaces/model', 
							 'app/lib'
							);
		$handler = '';
		$file = '';

		foreach($directories as $dir){

				// open the directory
				if ($handler = opendir($dir)) {
				    // list directory contents
				    while (false !== ($file = readdir($handler))) {
				        // only grab file names
				        if (is_file($dir .'/'. $file)) {
				            
				            require_once $dir.'/'.$file;
				        }
				    }
				    closedir($handler);
				}
		}	
		require_once'config/database.php';
	}
}

$require = new required();

?>