<?php

class database_configuration{
	private $config = NULL;

	function __Construct(){
		/**
			database configuration
		**/
		$configuration = array(
		    
		    'driver'    => 'mysqli',
		    'host'      => '127.0.0.1',
		    'database'  => 'sales',
		    'username'  => 'root',
		    'password'  => ''
		);

		$this->config = $configuration;
	}

	public function getDbConfiguration(){

		return $this->config;
	}	
}
?>


